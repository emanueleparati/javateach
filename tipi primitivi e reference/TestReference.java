public class TestReference
{
	public static void main(String[] args)
	{
		double unNumero = 5.0;
		MyDate dt = new MyDate(26, 1, 2015);
		
		System.out.println("Il numero vale: " + unNumero);
		System.out.println("La data rappresenta il giorno: " + dt.asText());
	}
}
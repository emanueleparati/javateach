public class TestTipiPrimitivi
{
	public static void main(String[] args)
	{
		// Promozione di espressioni che coinvolgono interi è sempre su int (no su long)
		// Le costanti intere sono int
		
		//byte b = 127;
		//byte b = 128;
		//int b = 128; //128 per il compilatore è int
		byte b = 50;
		//b = b*2; //b*2 viene convertito dal compilatore in int
		b = (byte) (b*3);
		System.out.println("Valore di b:" + b);
				
		int i = 2147483647;
		i = i+1;
		System.out.println("Valore di i:" + i);
		
		long l = 2147483648L;
		System.out.println("Valore di l:" + l);

		// Promozione di espressioni che coinvolgono variabi in virgola mobile è sempre su double
		// Le costanti in virgola mobile sono double
		//float pi = 3.14; // Errore
		//float pi = (float)3.14; // OK
		//float pi = 3.14F; // OK
		double pi = 3.14; // OK
		System.out.println("Valore di pi:" + pi);
		
		double d1 = -10.0 / 0.0;
		System.out.println("Valore di d:" + d1);
		
		double d2 = 0.0 / 0.0;
		System.out.println("Valore di d:" + d2);

		char phi = '\u03A6';
		System.out.println("Valore di phi:" + phi);

		char cir = '\u046C';
		System.out.println("Valore di cir:" + cir);
		
		// Oltre i 16bit occorre ricorrere ad una classe specifica (Character)
		char[] facc = Character.toChars(0x1F614);
		System.out.println("Faccina Unicode: " + new String(facc));

		// Caratteri in espressioni aritmetiche
		char ca = 'a';
		System.out.println("Valore di ca: " + ca);
		ca = (char) (ca + 1);
		System.out.println("Valore di ca + 1: " + ca);
		char phipiuuno = (char)(phi + 1);
		System.out.println("Valore di phi + 1: " + phipiuuno);
		
	}
}
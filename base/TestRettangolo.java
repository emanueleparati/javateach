public class TestRettangolo
{
	public static void main(String[] args)
	{
		Rettangolo r1 = new Rettangolo(4,2);
		
		int p1 = r1.perimetro();
		int a1 = r1.area();
		
		System.out.println("Perimetro del primo quadrato (" + r1.altezza + "x" + r1.larghezza + "): " + p1);
		System.out.println("Area del primo quadrato: " + a1);

		Rettangolo r2 = new Rettangolo(7,4);
		
		int p2 = r2.perimetro();
		int a2 = r2.area();
		
		System.out.println("Perimetro del secondo quadrato (" + r2.altezza + "x" + r2.larghezza + "): " + p2);
		System.out.println("Area del secondo quadrato: " + a2);
	}
}
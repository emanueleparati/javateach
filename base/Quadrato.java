public class Quadrato
{
	public int lato = 0;
	
	public Quadrato(int l)
	{
		lato = l;
	}
	public int perimetro()
	{
		return lato*4;
	}
}
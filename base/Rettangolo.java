public class Rettangolo
{
	public int altezza = 0;
	public int larghezza = 0;
	
	public Rettangolo(int a, int l)
	{
		altezza = a;
		larghezza = l;
	}
	public int perimetro()
	{
		int p = altezza*2 + larghezza*2;
		return p;
	}
	public int area()
	{
		int a = altezza*larghezza;
		return a;
	}
}
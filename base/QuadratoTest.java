public class QuadratoTest
{
	public static void main(String[] args)
	{
		Quadrato q1 = new Quadrato(10);
		Quadrato q2 = new Quadrato(7);
		
		int p1 = q1.perimetro();
		int p2 = q2.perimetro();
		
		System.out.println("Perimetro del primo quadrato: " + p1);
		System.out.println("Perimetro del secondo quadrato: " + p2);
	}
}